using Shygen.Example.Modules.Player.Model;
using UnityEngine;

namespace Shygen.Example.Scripts
{
    public class Example : MonoBehaviour
    {
        private void Awake()
        {
            var model = new PlayerModel();
            
            model.Invalidate();

            var copy = model.Copy();
        }
    }
}