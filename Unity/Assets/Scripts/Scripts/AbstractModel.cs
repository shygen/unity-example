namespace Shygen.Example.Scripts
{
    public class AbstractModel
    {
        public bool HasChanged { get; protected set; }
    }
}