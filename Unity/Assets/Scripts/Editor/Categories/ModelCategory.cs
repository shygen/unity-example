#region Disclaimer
// <copyright file="ModelCategory.cs">
// Copyright (c) 2019 - 2019 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using Shygen.Editor;
using Shygen.Editor.Attributes;
using Shygen.Editor.Interfaces;
using Shygen.Example.Generation.Categories;

namespace Shygen.Example.Editor.Categories
{
    public class ModelDataDraft : DataDraft
    {
        public class Property
        {
            public string Name;
            public Type Type;
        }
        
        public List<Property> Properties { get; private set; }
        
        public override void Initialize(Type type)
        {
            base.Initialize(type);
            
            Properties = new List<Property>();
            FieldInfo[] fields = type.GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
            foreach (FieldInfo field in fields)
            {
                var prop = new Property();
                prop.Name = field.Name;
                prop.Type = field.FieldType;
                Properties.Add(prop);
            }
        }
    }

    // All of these attribute are confusing and the user has to know about them
    [DataDraftType(typeof(ModelDataDraft))]
    [RuntimeAttributeType(typeof(ModelCategoryAttribute))]
    [DisplayName("Model")]
    public class ModelCategory : ICodeGenerationCategory
    {
        public string Name => "Model";

        public string Namespace => "";

        public Type DataDraftType => typeof(ModelDataDraft);

        public Type RuntimeAttributeType => typeof(ModelCategoryAttribute);
    }
}