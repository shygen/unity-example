using System.Globalization;
using System.Text;
using RobinBird.Logging.Runtime;
using UnityEditor;

namespace Shygen.Example.Editor
{
    /// <summary>
    /// Representation of a classical generation approach
    /// </summary>
    public static class TagGenerator
    {
        [MenuItem("Tools/Generate Tags")]
        public static void Generate()
        {
            string[] tags = UnityEditorInternal.InternalEditorUtility.tags;
            var sb = new StringBuilder();

            sb.Append(
@"namespace Shygen.Example
{
    public class UnityTags
    {
        ");

            foreach (string tag in tags)
            {
                sb.Append("public const string ");
                sb.Append(tag.ToUpper(CultureInfo.InvariantCulture));
                sb.Append(" = \"");
                sb.Append(tag);
                sb.AppendLine("\";");
            }

            sb.Append(@"
    }
}
");
            Log.Info(sb.ToString());
        }
    }
}