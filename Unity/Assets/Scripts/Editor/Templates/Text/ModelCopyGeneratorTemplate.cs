using RobinBird.Utilities.Runtime.Extensions;
using Shygen.Editor.Generators;

using Shygen;
using Shygen.Editor;
using Shygen.Example.Editor.Categories;
using Shygen.Runtime.CSharpGeneration;


namespace Shygen.Example.Editor.Templates.Text
{
    public class ModelCopyGeneratorTemplate : InlineTemplate<ModelDataDraft>
    {
        
    }
}





public class TempModelCopyGeneratorTemplate : Shygen.Example.Editor.Templates.Text.ModelCopyGeneratorTemplate
{
    protected override void InternalGenerate()
    {
        base.InternalGenerate();
        Output(@"

namespace ");
        Output(ModuleNamespace);
        Output(@"Model
{
    using System;

    ");
        string typeName = Data.Name;
        Output(@"

    public static class ");
        Output(Data.Name);
        Output(@"Copy
    {
        public static ");
        Output(Data.Name);
        Output(@" Copy(");
        Output(Data.Name);
        Output(@" original)
        {
            ");
        Output(typeName);
        Output(@" data = new ");
        Output(typeName);
        Output(@"();
            ");
        for (int i = 0; i < Data.Properties.Count; i++)
        {
            var prop = Data.Properties[i];
            Output(@"
                data.");
            Output(prop.Name.ToFirstUpperChar());
            Output(@" = original.");
            Output(prop.Name.ToFirstUpperChar());
            Output(@";
             ");
        }
        Output(@"
             return data;
        }
    }
}
");

    }

}