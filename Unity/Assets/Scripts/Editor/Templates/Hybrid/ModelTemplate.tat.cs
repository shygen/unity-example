﻿using System;
using System.ComponentModel;
using System.Text;
using RobinBird.Utilities.Runtime.Extensions;
using Shygen.Example.Editor.Categories;
using Shygen.Example.Modules.ModuleName.Model;
using UnityEngine.Experimental.UIElements;

namespace Shygen.Example.Editor.Templates.Hybrid
{
    using Shygen.Editor.Generators;
    using Runtime.Attributes;

    [TemplateName("Model")]
    public class ModelGenerator : CSharpGenerator<ModelDataDraft>
    {
        protected override IterationMode Mode => IterationMode.Single;
        
        protected override void ModifyTemplate(CSharpTemplate template)
        {
            template.Replace(nameof(Modules.ModuleName), ModuleName);
            template.Replace(nameof(DataModel), Data.Name);

            foreach (ModelDataDraft.Property property in Data.Properties)
            {
                var group = template.GetGroupInstance("dataProperty");

                group.Replace(nameof(Int32), property.Type.FullName);
                group.Replace(nameof(DataModel.MyData), property.Name.ToFirstUpperChar());
                group.Replace("myData", property.Name);

                template.InsertAfterGroup("dataProperty", group.Output);
                
                var invalidateGroup = template.GetGroupInstance("invalidateData");

                invalidateGroup.Replace("myData", property.Name);
                invalidateGroup.Replace("0",
                    property.Type.IsValueType ? Activator.CreateInstance(property.Type).ToString() : "null");

                template.InsertAfterGroup("invalidateData", invalidateGroup.Output);
            }
        }
    }
}

//%<template provider="Shygen.Example.Editor.Templates.Hybrid.ModelGenerator">
namespace Shygen.Example.Modules.ModuleName.Model
{
    using Shygen.Example.Scripts;
    
    public partial class DataModel : AbstractModel
    {
        //%<group name="dataField">
        private Int32 myData;
        //%</group>
        
        //%<group name="dataProperty">
        
        public Int32 MyData
        {
            get { return myData; }
            set {
                if (myData != value)
                {
                    myData = value;
                    HasChanged = true;
                }

                 }
        }
            
        
        //%</group>
        
        public void Invalidate()
        {
            //%<group name="invalidateData">
            myData = 0;
            //%</group>
        }
    }
}
//%</template>