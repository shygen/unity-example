﻿using JetBrains.Annotations;
using Shygen.Example.Editor.Categories;
using Shygen.Runtime.Attributes;

namespace Shygen.Example.Editor
{
    [GroupConfiguration("Example", "Shygen.Example.Modules", "Assets/Scripts/Modules/GeneratedCode", "Player")]

    [TemplateConfiguration(typeof(ModelCategory), "Assets/Scripts/Editor/Templates/Hybrid/ModelTemplate.tat.cs")]
    [TemplateConfiguration(typeof(ModelCategory), "Assets/Scripts/Editor/Templates/Text/ModelCopyGenerator.tat")]
    [UsedImplicitly]
    public class GenerationConfiguration
    {
    }
}
