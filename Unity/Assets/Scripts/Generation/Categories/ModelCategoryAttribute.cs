#region Disclaimer
// <copyright file="ModelCategoryAttribute.cs">
// Copyright (c) 2019 - 2019 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>
#endregion

using Shygen.Runtime.Attributes;

namespace Shygen.Example.Generation.Categories
{
    public class ModelCategoryAttribute : CategoryAttribute
    {
        public ModelCategoryAttribute(string name) : base(name)
        {
        }
    }
}