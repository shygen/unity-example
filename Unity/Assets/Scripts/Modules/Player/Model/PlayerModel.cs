using Shygen.Example.Generation.Categories;
using Shygen.Runtime.Attributes;

namespace Shygen.Example.Modules.Player.Model
{
     [Generate(moduleName: "Player",
         typeof(ModelCategoryAttribute))]
     public partial class PlayerModel
     {
         private string name;

         private int diamonds;

         private float gender;
     }
}