

namespace Shygen.Example.Modules.Player.Model
{
    using System;

    

    public partial class PlayerModel
    {
        public static PlayerModel Copy(PlayerModel original)
        {
            PlayerModel data = new PlayerModel();
            
                data.name = original.name;
             
                data.diamonds = original.diamonds;
             
                data.gender = original.gender;
             
             return data;
        }
        
        public PlayerModel Copy()
        {
            PlayerModel data = new PlayerModel();
            
                data.name = this.name;
             
                data.diamonds = this.diamonds;
             
                data.gender = this.gender;
             
             return data;
        }
    }
}
