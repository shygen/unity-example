
namespace Shygen.Example.Modules.Player.Model
{
    using Shygen.Example.Scripts;
    
    public partial class PlayerModel : AbstractModel
    {
        public System.Single Gender
        {
            get { return gender; }
            set {
                if (gender != value)
                {
                    gender = value;
                    HasChanged = true;
                }

                 }
        }public System.Int32 Diamonds
        {
            get { return diamonds; }
            set {
                if (diamonds != value)
                {
                    diamonds = value;
                    HasChanged = true;
                }

                 }
        }public System.String Name
        {
            get { return name; }
            set {
                if (name != value)
                {
                    name = value;
                    HasChanged = true;
                }

                 }
        }
        
        public void Invalidate()
        {
            gender = 0;diamonds = 0;name = null;
        }
    }
}
